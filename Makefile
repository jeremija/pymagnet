PATH := $(CURDIR)/env/bin:$(PATH)
SHELL := sh

ci: deps lint test-coverage

lint: FORCE
	flake8 magnet/
	mypy magnet/

test: FORCE
	pytest $(args)

test-watch: FORCE
	bash -c '. activate && ptw magnet/ $(args)'

test-coverage: FORCE
	pytest --cov-branch --cov-report term-missing --cov=magnet/ $(args)

deps: env FORCE
	pip install -r requirements.txt

freeze: FORCE
	pip freeze > requirements.txt

version-patch: FORCE
	bumpversion patch

version-minor: FORCE
	bumpversion minor

version-major: FORCE
	bumpversion major

env:
	python3 -m venv env

build: FORCE
	$(MAKE) -f Makefile.build build

build-deps: FORCE
	$(MAKE) -f Makefile.build deps

upload: FORCE
	$(MAKE) -f Makefile.build upload

FORCE:
